import React, { useState, useEffect } from "react";
import "./Enchere.css";
import Axios from "axios";
import { IonButton, IonButtons, IonCard, IonCardContent, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import axios from "axios";
import Menu from "./Menu";
import Adresse from "./Adresse";



function Enchere() {
  const [categorieList, setCategorieList] = useState([]);
  const [idCategorie, setIdCategorie] = useState(0);
  const [nom, setNom] = useState("");
  const [dateHeureDebut, setdateHeureDebut] = useState("");
  const [dateHeureFin, setdateHeureFin] = useState("");
  const [prix, setPrix] = useState("");

  useEffect(() => {
    if (localStorage.getItem("value") == null) {
      window.location.replace("/")
    }
    Axios.get(Adresse() + "/categorie").then((response) => {
      setCategorieList(response.data);
    });
  }, []);

  const handleChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = event.target.files;
    if (files) {
      const formData = new FormData();
      Array.from(files).forEach((file: File) => {
        formData.append("images", file);
      });
      try {
        const response = await axios.post(
            Adresse() + "/upload",
          formData
        );
        console.log(response);
      } catch (error) {
        console.error(error);
      }
    }
  };

  const submitReview = () => {
    Axios.post( Adresse() +  "/insert", {
      idCategorie: idCategorie,
      nom: nom,
      dateHeureDebut: dateHeureDebut,
      dateHeureFin: dateHeureFin,
      prix: prix,
    }).then(() => {
      alert("succesful insert");
    });
  };

  return (
    <>
      <Menu />
      <IonPage id="main-content">
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonMenuButton></IonMenuButton>
            </IonButtons>
            <IonTitle>Ajouter Enchere</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-padding">
          <IonCard>
            <IonCardContent>
              <div className="Enchere">
                <h1>Ajouter l'enchère</h1>

                <div className="form">
                  Catégorie :
                  <select
                    name="idCategorie"
                    onChange={(e) => {
                      setIdCategorie(parseInt(e.target.value));
                    }}
                  >
                    <option value="">Choisissez la catégorie</option>

                    {categorieList.map((val, index) => {
                      return (
                        <option key={val["id"] || index} value={val["id"]}>
                          {val["typeCate"]}
                        </option>
                      );
                    })}
                  </select>
                  Nom du produit:
                  <input
                    type="text"
                    name="nom"
                    onChange={(e) => {
                      setNom(e.target.value);
                    }}
                  />
                  Date et heure début :
                  <input
                    type="datetime-local"
                    name="dateHeureDebut"
                    onChange={(e) => {
                      setdateHeureDebut(e.target.value);
                    }}
                  />
                  Date et heure fin :
                  <input
                    type="datetime-local"
                    name="dateHeureFin"
                    onChange={(e) => {
                      setdateHeureFin(e.target.value);
                    }}
                  />
                  Prix de départ :
                  <input
                    type="text"
                    name="prix"
                    onChange={(e) => {
                      setPrix(e.target.value);
                    }}
                  />
                  Insérer une photo :
                  <input
                    type="file"
                    //name="photo"
                    onChange={handleChange}
                    multiple={true}
                  />
                  <IonButton onClick={submitReview}>Ajouter</IonButton>
                </div>
              </div>
            </IonCardContent>
          </IonCard>
        </IonContent>
      </IonPage>
    </>
  );
}

export default Enchere;

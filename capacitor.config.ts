import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'enchere',
  appName: 'enchere',
  webDir: 'build',
  bundledWebRuntime: false,
  plugins: {
    PushNotifications: {
      presentationOptions: ["badge", "sound", "alert"],
    },
  },
};

export default config;
